package com.cycle;


import com.cycle.components.*;

import java.time.LocalDateTime;

public class Operations {

    Double totalPrice;
    StringBuilder message;

    public void assembleCycleBasedOnRequirement(boolean powerBreak, boolean withRearShock, boolean tubeless,
                                                LocalDateTime localDateTime, Stores stores, int numberOfCycle) throws Exception {
        if (stores == null)
            throw new Exception("Store not found !");
        if (numberOfCycle <= 0)
            throw new Exception("provide the number of the cycle !");
        totalPrice = new Double(0.0);
        message = new StringBuilder("");

        if (stores.getBreaks().size() < numberOfCycle)
            throw new Exception("Breaks are not available as per expected ");
        stores.getBreaks().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice <= 0)
            throw new Exception("Break is not available as specified !");
        message.append("Break Price : " + totalPrice + "\n");
        Double lastStorePrice = totalPrice;

        if (stores.getChainAssemblies().size() < numberOfCycle)
            throw new Exception("Chains are not available as per expected ");
        stores.getChainAssemblies().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Chains is not available as specified !");
        message.append("Chain Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getFrames().size() < numberOfCycle)
            throw new Exception("Frames are not available as per expected ");
        stores.getFrames().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Frame is not available as specified !");
        message.append("Frame Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getHandles().size() < numberOfCycle)
            throw new Exception("Handles are not available as per expected ");
        stores.getHandles().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Handles is not available as specified !");
        message.append("Handles Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getHubs().size() < numberOfCycle)
            throw new Exception("Hubs are not available as per expected ");
        stores.getHubs().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Hubs is not available as specified !");
        message.append("Hubs Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getRearShocks().size() < numberOfCycle)
            throw new Exception("RearShock are not available as per expected ");
        stores.getRearShocks().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("RearShock is not available as specified !");
        message.append("RearShock Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getRims().size() < numberOfCycle)
            throw new Exception("Rim are not available as per expected ");
        stores.getRims().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Rim is not available as specified !");
        message.append("Rim Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getSeatings().size() < numberOfCycle)
            throw new Exception("Seatings are not available as per expected ");
        stores.getSeatings().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Seatings is not available as specified !");
        message.append("Seatings Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getSpokes().size() < numberOfCycle)
            throw new Exception("Spokes are not available as per expected ");
        stores.getSpokes().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Spokes is not available as specified !");
        message.append("Spokes Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getTubes().size() < numberOfCycle)
            throw new Exception("Tubes are not available as per expected ");
        stores.getTubes().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Tubes is not available as specified !");
        message.append("Tubes Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getTyres().size() < numberOfCycle)
            throw new Exception("Tyres are not available as per expected ");
        stores.getTyres().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("Tyres is not available as specified !");
        message.append("Tyres Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        if (stores.getWheels().size() < numberOfCycle)
            throw new Exception("wheels are not available as per expected ");
        stores.getWheels().stream().parallel().forEach(c -> calculateBreakPrice(c, powerBreak, withRearShock, tubeless, localDateTime));
        if (totalPrice - lastStorePrice <= 0)
            throw new Exception("wheels is not available as specified !");
        message.append("wheels Price : " + (totalPrice - lastStorePrice) + "\n");
        lastStorePrice = totalPrice;

        System.out.println(message);
    }

    private void calculateBreakPrice(BaseConfiguration b, boolean powerBreak, boolean withRearShock, boolean tubeless, LocalDateTime localDateTime) {
        if (b.getPriceVaryFrom().equals(localDateTime) || b.getPriceVaryFrom().isBefore(localDateTime)) {
            if (b.getClass().isInstance(Break.class)) {
                Break c = (Break) b;
                totalPrice = ((powerBreak && c.isPowerBreak()) || (!powerBreak && !c.isPowerBreak())) ?
                        totalPrice + b.getPrice() : totalPrice;
            } else if (b.getClass().isInstance(Frame.class)) {
                Frame c = (Frame) b;
                totalPrice = ((withRearShock && c.isWithRearShock()) || (!withRearShock && !c.isWithRearShock())) ?
                        totalPrice + b.getPrice() : totalPrice;
            } else if (b.getClass().isInstance(Tyre.class)) {
                Tyre c = (Tyre) b;
                totalPrice = ((tubeless && c.isTubeless()) || (!tubeless && !c.isTubeless())) ?
                        totalPrice + b.getPrice() : totalPrice;
            } else
                totalPrice += b.getPrice();
        }
    }
}
