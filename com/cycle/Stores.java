package com.cycle;

import com.cycle.components.*;

import java.util.ArrayList;
import java.util.List;

public class Stores {
    private String storeName;
    private List<Break> breaks = new ArrayList<>();
    private List<ChainAssembly> chainAssemblies = new ArrayList<>();
    private List<Frame> frames = new ArrayList<>();
    private List<Handle> handles = new ArrayList<>();
    private List<Hub> hubs = new ArrayList<>();
    private List<RearShock> rearShocks = new ArrayList<>();
    private List<Rim> rims = new ArrayList<>();
    private List<Seating> seatings = new ArrayList<>();
    private List<Spoke> spokes = new ArrayList<>();
    private List<Tube> tubes = new ArrayList<>();
    private List<Tyre> tyres = new ArrayList<>();
    private List<Wheel> wheels = new ArrayList<>();

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public List<Break> getBreaks() {
        return breaks;
    }

    public void setBreaks(List<Break> breaks) {
        this.breaks.addAll(breaks);
        this.breaks.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<ChainAssembly> getChainAssemblies() {
        return chainAssemblies;
    }

    public void setChainAssemblies(List<ChainAssembly> chainAssemblies) {
        this.chainAssemblies.addAll(chainAssemblies);
        this.chainAssemblies.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Frame> getFrames() {
        return frames;
    }

    public void setFrames(List<Frame> frames) {
        this.frames.addAll(frames);
        this.frames.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Handle> getHandles() {
        return handles;
    }

    public void setHandles(List<Handle> handles) {
        this.handles.addAll(handles);
        this.handles.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Hub> getHubs() {
        return hubs;
    }

    public void setHubs(List<Hub> hubs) {
        this.hubs.addAll(hubs);
        this.hubs.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<RearShock> getRearShocks() {
        return rearShocks;
    }

    public void setRearShocks(List<RearShock> rearShocks) {
        this.rearShocks.addAll(rearShocks);
        this.rearShocks.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Rim> getRims() {
        return rims;
    }

    public void setRims(List<Rim> rims) {
        this.rims.addAll(rims);
        this.rims.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Seating> getSeatings() {
        return seatings;
    }

    public void setSeatings(List<Seating> seatings) {
        this.seatings.addAll(seatings);
        this.seatings.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Spoke> getSpokes() {
        return spokes;
    }

    public void setSpokes(List<Spoke> spokes) {
        this.spokes.addAll(spokes);
        this.spokes.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Tube> getTubes() {
        return tubes;
    }

    public void setTubes(List<Tube> tubes) {
        this.tubes.addAll(tubes);
        this.tubes.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Tyre> getTyres() {
        return tyres;
    }

    public void setTyres(List<Tyre> tyres) {
        this.tyres.addAll(tyres);
        this.tyres.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels.addAll(wheels);
        this.wheels.sort((o1, o2) -> o2.getPriceVaryFrom().compareTo(o1.getPriceVaryFrom()));
    }
}
