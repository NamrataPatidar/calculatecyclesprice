package com.cycle;

import java.time.LocalDateTime;

public class Test {
    public static void main(String[] args) {
        try {
            //Create the store first
            Stores stores = new Stores();
            //After creating store we can fill the components data now
            stores.setStoreName("Namrata Cycle Store !");


            //Create object of store table and perform the operations
            Operations operations = new Operations();
            operations.assembleCycleBasedOnRequirement(true, true, true, LocalDateTime.now(), stores, 1000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
