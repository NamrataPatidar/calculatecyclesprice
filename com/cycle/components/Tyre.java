package com.cycle.components;

public class Tyre extends BaseConfiguration {
    private boolean tubeless;
    private Tube tube;

    @Override
    public double getPrice() {
        if (tubeless)
            return price;
        return price + tube.getPrice();
    }

    public boolean isTubeless() {
        return tubeless;
    }

    public void setTubeless(boolean tubeless) {
        this.tubeless = tubeless;
    }

    public Tube getTube() {
        return tube;
    }

    public void setTube(Tube tube) {
        this.tube = tube;
    }
}
