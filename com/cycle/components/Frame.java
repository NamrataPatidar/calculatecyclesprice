package com.cycle.components;

public class Frame extends BaseConfiguration {
    private boolean withRearShock;
    private RearShock rearShock;

    @Override
    public double getPrice() {
        if (withRearShock)
            return price + rearShock.getPrice();
        else
            return price;
    }

    public boolean isWithRearShock() {
        return withRearShock;
    }

    public void setWithRearShock(boolean withRearShock) {
        this.withRearShock = withRearShock;
    }
}
