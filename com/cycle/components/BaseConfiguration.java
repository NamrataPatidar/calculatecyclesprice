package com.cycle.components;

import java.time.LocalDateTime;

public class BaseConfiguration {
    protected LocalDateTime priceVaryFrom;
    protected double price;
    protected String partName;


    public LocalDateTime getPriceVaryFrom() {
        return priceVaryFrom;
    }

    public void setPriceVaryFrom(LocalDateTime priceVaryFrom) {
        this.priceVaryFrom = priceVaryFrom;
    }

    public void reconfiguredPrice(double newPrice) {
        this.price = newPrice;
        this.priceVaryFrom = LocalDateTime.now();
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }
}
