package com.cycle.components;

public class Wheel extends BaseConfiguration {
    private Spoke spoke;
    private Hub hub;
    private Tyre tyre;
    private Rim rim;
    private double price;

    public Spoke getSpoke() {
        return spoke;
    }

    public void setSpoke(Spoke spoke) {
        this.spoke = spoke;
    }

    public Hub getHub() {
        return hub;
    }

    public void setHub(Hub hub) {
        this.hub = hub;
    }

    public Tyre getTyre() {
        return tyre;
    }

    public void setTyre(Tyre tyre) {
        this.tyre = tyre;
    }

    public Rim getRim() {
        return rim;
    }

    public void setRim(Rim rim) {
        this.rim = rim;
    }

    public double getPrice() {
        return price + spoke.getPrice() + hub.getPrice() + tyre.getPrice() + rim.getPrice();
    }
}
