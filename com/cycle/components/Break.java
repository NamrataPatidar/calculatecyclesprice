package com.cycle.components;

public class Break extends BaseConfiguration {
    private boolean powerBreak;

    public boolean isPowerBreak() {
        return powerBreak;
    }

    public void setPowerBreak(boolean powerBreak) {
        this.powerBreak = powerBreak;
    }


}
