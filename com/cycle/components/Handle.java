package com.cycle.components;

public class Handle extends BaseConfiguration {
    private Break breaks;

    @Override
    public double getPrice() {
        return price + breaks.getPrice();
    }

    public Break getBreaks() {
        return breaks;
    }

    public void setBreaks(Break breaks) {
        this.breaks = breaks;
    }
}
