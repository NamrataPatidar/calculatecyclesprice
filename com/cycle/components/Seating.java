package com.cycle.components;

public class Seating extends BaseConfiguration {
    private String colour;

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
}
