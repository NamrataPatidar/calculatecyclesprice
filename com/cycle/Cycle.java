package com.cycle;


import com.cycle.components.*;

public class Cycle {
    /**
     * We can create more components here.
     */
    private Frame frame;
    private Handle handle;
    private Seating seating;
    private Wheel wheel;
    private ChainAssembly chainAssembly;
    private double cyclePrice;

    public Frame getFrame() {
        return frame;
    }

    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public Handle getHandle() {
        return handle;
    }

    public void setHandle(Handle handle) {
        this.handle = handle;
    }

    public Seating getSeating() {
        return seating;
    }

    public void setSeating(Seating seating) {
        this.seating = seating;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public ChainAssembly getChainAssembly() {
        return chainAssembly;
    }

    public void setChainAssembly(ChainAssembly chainAssembly) {
        this.chainAssembly = chainAssembly;
    }

    public double getCyclePrice() {
        return cyclePrice + frame.getPrice() + handle.getPrice() + seating.getPrice() + wheel.getPrice() + chainAssembly.getPrice();
    }

    public void setCyclePrice(double cyclePrice) {
        this.cyclePrice = cyclePrice;
    }
}
